Faire un dump SQL d'une bdd existante pour créer/lancer une bdd via Docker
==========================================================================

- Préliminaires :

```bash
TARGET_DIR=/tmp/sygal-db
mkdir -p ${TARGET_DIR}/sql/admin
```

- Exemple de dump d'une base de prod nommée "sygal" en vue de créer une base de développement :

```bash
PGPASSWORD='xxxxxx' pg_dump -h localhost -p 5432 -d "sygal" -U "sygal" > ${TARGET_DIR}/sql/sygal_prod_dump.sql
```

NB : le dump doit avoir l'extension `.sql`.

- Cherchez dans le dump si des instructions "OWNER TO" font référence à un user particulier :

```bash
grep -Ens "OWNER TO" ${TARGET_DIR}/sql/* | head -n 1
```

Si un user est mentionné, c'est lui qui devra être spécifié au moment du `docker run` pour qu'il soit
créé dans la base de données (cf. plus bas).

L'autre solution est de modifier le user DANS le dump s'il ne vous convient pas.

- Copiez le script de création de la bdd et du user :

```bash
cp ./docker/sql/admin/01_create_db_user.sql ${TARGET_DIR}/sql/admin/
```

- Lancer le container en adaptant `--env SYGAL_USER=sygal` au user trouvé précédemment :

```bash
docker run \
--rm \
--env POSTGRES_USER=postgres \
--env POSTGRES_PASSWORD=admin \
--env SYGAL_DB=sygal \
--env SYGAL_USER=sygal \
--env SYGAL_PASSWORD=azerty \
--publish 5432:5432 \
--volume ${TARGET_DIR}/sql/:/sql \
--volume ${TARGET_DIR}/db:/var/lib/postgresql/data \
sygal-db-image
```

Au 1er lancement, si aucune bdd n'est déjà persistée dans le répertoire `${TARGET_DIR}/db`, les scripts SQL sont 
exécutés : la bdd est créée.

Aux lancements suivants, la bdd existe dans le répertoire donc les scripts ne sont pas exécutés.
Pour que la bdd soit re-créée, il faut supprimer le répertoire `${TARGET_DIR}/db`.
