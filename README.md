SyGAL Database Image
====================

Ce projet est une image Docker rassemblant le nécessaire pour créer (le cas échéant) et lancer une base de données 
pour l'application ESUP-SyGAL.

--------

Pour une bdd **neuve** (dist)
-----------------------------

### Mise à jour (facultative)

Pour récupérer dans les sources de SyGAL la dernière version des scripts SQL de création d'un bdd **neuve** et les
copier ici, lancer cette commande :
```bash
./docker/update_dist.sh -g https://git.unicaen.fr/open-source/sygal.git
```

N'oubliez pas de mettre à jour le dépôt si névessaire :
```shell
git add . && git commit -m "Mise à jour des scripts de création d'une bdd neuve." && git push
```

### Build de l'image

```bash
docker compose build sygal-db-dist
```

**Attention !** Si vous êtes derrière un proxy, vous pouvez transmettre les valeurs courantes de vos variables d'environement `*_proxy`
à la commande `build` avec des `--build-arg` additionnels.

### Lancement

```bash
docker compose up sygal-db-dist
```

ou alors

```bash
docker run \
--rm \
--env POSTGRES_USER=postgres \
--env POSTGRES_PASSWORD=admin \
--env SYGAL_DB=sygal \
--env SYGAL_USER=ad_sygal \
--env SYGAL_PASSWORD=azerty \
--publish 5432:5432 \
--volume $PWD/docker/dist/sql/:/sql \
--volume $PWD/docker/dist/db:/var/lib/postgresql/data \
sygal-db-dist
```

Remarques :
  - Les variables d'environnement `POSTGRES_*` spécifient le super-utilisateur à créer 
    (cf. https://registry.hub.docker.com/_/postgres).
  - Les variables d'environnement `SYGAL_*` spécifient la base de données et l'utilisateur SyGAL à créer.
  - Le montage `$PWD/data/db:/var/lib/postgresql/data` permet de spécifier un répertoire dans lequel sera persistée 
    la base de données.
    Si aucune base n'est déjà persistée, le script `/initdb.sh` est lancé automatiquement 
    au démarrage du container pour exécuter les scripts SQL situés dans le répertoire `/sql` :
    - Pour commencer, ce sont les scripts SQL placés dans le sous-répertoire `admin` qui sont exécutés
      **avec le super-user identifié par les variables d'environnement `POSTGRES_USER` et `POSTGRES_PASSWORD`**. 
    - Ce sont ensuite les autres scripts qui sont exécutés avec le user identifié par les variables d'environnement 
      `SYGAL_USER` et `SYGAL_PASSWORD`.
  - Vous pouvez substituer les scripts SQL par les vôtres en remplaçant le montage dans `/sql` 
    (exemple : `--volume $PWD/mes/scripts/sql/:/sql`). La présence d'un sous-répertoire `admin` n'est pas obligatoire. 


### Utilisation de l'image dans un `docker-compose.yml`

L'image doit avoir été buildée au préalable.

```yml
version: '2.2'
services:
  sygal-db-dist:
    image: sygal-db-dist
    restart: always
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: admin
      SYGAL_DB: sygal
      SYGAL_USER: ad_sygal
      SYGAL_PASSWORD: azerty
    ports:
      - "5432:5432"
    volumes:
      - ./mon/emplacement/db:/var/lib/postgresql/data # répertoire où la bdd est persistée
```


--------


Pour une bdd de **démo**
------------------------

### Mise à jour (facultative)

```bash

```

### Build

```bash
docker build \
--rm \
-t sygal-db-demo \
.
```

### Lancement

```bash
docker compose up sygal-db-demo
```

ou alors

```bash
docker run \
--rm \
--env POSTGRES_USER=postgres \
--env POSTGRES_PASSWORD=admin \
--env SYGAL_DB=sygal_demo \
--env SYGAL_USER=ad_sygal_demo \
--env SYGAL_PASSWORD=azerty \
--publish 5432:5432 \
--volume $PWD/docker/demo/sql/:/sql \
--volume $PWD/docker/demo/db:/var/lib/postgresql/data \
sygal-db-demo
```

### Utilisation de l'image dans un `docker-compose.yml`

L'image doit avoir été buildée au préalable.

```yml
version: '2.2'
services:
  sygal-db-demo:
    image: sygal-db-demo
    restart: always
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: admin
      SYGAL_DB: sygal_demo
      SYGAL_USER: ad_sygal_demo
      SYGAL_PASSWORD: azerty
    ports:
      - "5432:5432"
    volumes:
      - ./mon/emplacement/db:/var/lib/postgresql/data # répertoire où la bdd est persistée
```
