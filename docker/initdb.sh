#!/bin/bash
set -e

SQL_DIR="/sql"

##
## Lancement des scripts SQL présents dans le répertoire `/sql`.
##

unset PGHOST PGPORT PGDATABASE PGUSER PGPASSWORD # précaution

#echo "SYGAL_DB = $SYGAL_DB"
#echo "SYGAL_USER = $SYGAL_USER"
#echo "SYGAL_PASSWORD = $SYGAL_PASSWORD"

#
# Exécution EN TANT QUE SUPER-USER de tous les scripts .sql présents dans le répertoire `/sql/admin`.
# NB : il est possible de substituer ces scripts par les vôtres grâce à un volume à monter dans `/sql/admin`.
#
if [ -d "${SQL_DIR}/admin" ]; then
  cd ${SQL_DIR}/admin
  export \
  PGDATABASE=$POSTGRES_DB \
  PGUSER=$POSTGRES_USER \
  PGPASSWORD=$POSTGRES_PASSWORD
  for f in *.sql; do
    [[ ! -f "$f" ]] && continue
    psql \
      -v ON_ERROR_STOP=1 \
      -v "dbname=${SYGAL_DB}" \
      -v "dbuser=${SYGAL_USER}" \
      -v "dbpassword='${SYGAL_PASSWORD}'" \
      -f $f
    [[ $? -ne 0 ]] && exit 1
  done
fi

#
# Exécution de tous les scripts .sql présents dans le répertoire `/sql`.
# NB : il est possible de substituer ces scripts par les vôtres grâce à un volume à monter dans `/sql`.
#
cd ${SQL_DIR}
export \
PGDATABASE=$SYGAL_DB \
PGUSER=$SYGAL_USER \
PGPASSWORD=$SYGAL_PASSWORD
for f in *.sql; do
  [[ ! -f "$f" ]] && continue
  psql \
    -v ON_ERROR_STOP=1 \
    -v "dbuser=${SYGAL_USER}" \
    -f $f
  [[ $? -ne 0 ]] && exit 1
done

unset PGHOST PGPORT PGDATABASE PGUSER PGPASSWORD # précaution
