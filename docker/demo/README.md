README
======

Ressources Docker pour une base de données de **démo**.




Re-création de la bdd après un `drop owned by USER` :

```bash
/usr/bin/psql --file=./docker/demo/sql/02_other/02_dump.sql --username=USER --host=localhost --port=54321 --dbname=sygal_demo
```
 