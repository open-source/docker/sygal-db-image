--
-- DATABASE, USER, ETC.
--

create database :dbname with ENCODING = 'UTF-8';
create user :dbuser with encrypted password :dbpassword NOSUPERUSER NOCREATEDB;
alter database :dbname owner to :dbuser;
grant all privileges on database :dbname to :dbuser;
