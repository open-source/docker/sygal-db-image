--
-- DONNÉES DE TEST.
--

--
-- Création de l'individu/utilisateur de test
--
insert into individu (id, nom_usuel, nom_patronymique, prenom1, email, source_code, supann_id, source_id, histo_createur_id)
select nextval('individu_id_seq'),
       $$Premier$$,
       $$Premier$$,
       $$François$$,
       'francois.premier@univ.fr',
       'UCN::00012345',
       '00012345',
       1,
       1
;
insert into utilisateur (id, individu_id, username, email, display_name, password, password_reset_token, nom, prenom)
select nextval('utilisateur_id_seq'),
       i.id,
       'francois.premier@univ.fr', -- un EPPN (si Shibboleth), ou un supannAliasLogin/uid (si LDAP) ou un email (si DB locale)
       'francois.premier@univ.fr',
       $$François Premier$$,
       '????', -- 'shib' (si authentification shibboleth), ou 'ldap' (si auth LDAP), ou mdp bcrypté (si local)
       'uCdWwSvPBWnlCtHWIgb8GLH3iGkKwrKSHoGqDjHTa6T9a8MeeYSD8aUdvcUvPjYu',
       $$Premier$$,
       $$François$$
from individu i
where i.source_code = 'UCN::00012345'
;

--
-- /!\ Attribution du rôle Admin tech à l'utilisateur de test !!
--
insert into individu_role(id, individu_id, role_id)
select nextval('individu_role_id_seq'), i.id, r.id
from individu i, role r
where i.source_code = 'UCN::00012345'
  and r.source_code = 'ADMIN_TECH'
;
