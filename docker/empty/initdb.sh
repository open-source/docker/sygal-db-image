#!/bin/bash
set -e

SQL_DIR="/sql"

##
## Lancement des scripts SQL présents dans le répertoire `/sql`.
##

echo "-------------------------------------------------------------------------------------------------------"
echo "                             $0"
echo "-------------------------------------------------------------------------------------------------------"

unset PGHOST PGPORT PGDATABASE PGUSER PGPASSWORD # précaution

[[ -z $POSTGRES_DB ]] && echo "Variable d'environnement POSTGRES_DB absente." && exit 1
[[ -z $POSTGRES_USER ]] && echo "Variable d'environnement POSTGRES_USER absente." && exit 1
[[ -z $POSTGRES_PASSWORD ]] && echo "Variable d'environnement POSTGRES_PASSWORD absente." && exit 1

[[ -z $SYGAL_DB ]] && echo "Variable d'environnement SYGAL_DB absente." && exit 1
[[ -z $SYGAL_USER ]] && echo "Variable d'environnement SYGAL_USER absente." && exit 1
[[ -z $SYGAL_PASSWORD ]] && echo "Variable d'environnement SYGAL_PASSWORD absente." && exit 1

echo "POSTGRES_DB = $POSTGRES_DB"
echo "POSTGRES_USER = $POSTGRES_USER"
echo "POSTGRES_PASSWORD = $POSTGRES_PASSWORD"
echo "SYGAL_DB = $SYGAL_DB"
echo "SYGAL_USER = $SYGAL_USER"
echo "SYGAL_PASSWORD = $SYGAL_PASSWORD"

#
# Exécution EN TANT QUE SUPER-USER de tous les scripts .sql présents dans le répertoire `/sql/01_admin`.
# NB : il est possible de substituer ces scripts par les vôtres grâce à un volume à monter dans `/sql/01_admin`.
#
if [ -d "${SQL_DIR}/01_admin" ]; then
  cd "${SQL_DIR}/01_admin"
  export \
    PGDATABASE=${POSTGRES_DB} \
    PGUSER=${POSTGRES_USER} \
    PGPASSWORD=${POSTGRES_PASSWORD}
  for f in *.sql; do
    [[ ! -f "$f" ]] && continue
    echo "--------------------------------------- $f ----------------------------------------"
    psql \
      -v ON_ERROR_STOP=1 \
      -v dbname=${SYGAL_DB} \
      -v dbuser=${SYGAL_USER} \
      -v dbpassword="'${SYGAL_PASSWORD}'" \
      -f $f
    [[ $? -ne 0 ]] && exit 1
  done
fi

echo

#
# Exécution de tous les scripts .sql présents dans le répertoire `/sql/02_other`.
# NB : il est possible de substituer ces scripts par les vôtres grâce à un volume à monter dans `/sql/02_other`.
#
if [ -d "${SQL_DIR}/02_other" ]; then
  cd "${SQL_DIR}/02_other"
  export \
    PGDATABASE=${SYGAL_DB} \
    PGUSER=${SYGAL_USER} \
    PGPASSWORD="'${SYGAL_PASSWORD}'"
  for f in *.sql; do
    [[ ! -f "$f" ]] && continue
    echo "--------------------------------------- $f ----------------------------------------"
    psql \
      -v ON_ERROR_STOP=1 \
      -v dbuser=${SYGAL_USER} \
      -f $f
    [[ $? -ne 0 ]] && exit 1
  done
fi

unset PGHOST PGPORT PGDATABASE PGUSER PGPASSWORD # précaution
