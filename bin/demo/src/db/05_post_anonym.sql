----------------------------------------------------------------------------------------------------------
-- Traitements complémentaires pour anonymisation.
----------------------------------------------------------------------------------------------------------

--
-- transformation des emails : {username}@...
--
update utilisateur u
set email = u.username || substr(u.email, position('@' in u.email))
from individu i
where u.individu_id = i.id
;

--
-- transformation des nom, prenom, display_name pour être cohérent avec l'individu lié éventuel
--
update utilisateur u
set nom = i.nom_usuel,
    prenom = i.prenom1,
    display_name = i.prenom1 || ' ' || i.nom_usuel
from individu i
where u.individu_id = i.id
;

--
-- anonymisation du npd dans les tables substit_* (non gérables par unicaen/db-anonym car sans entité Doctrine associée)
--
update substit_doctorant set npd = md5(random()::varchar);
update substit_ecole_doct set npd = md5(random()::varchar);
update substit_etablissement set npd = md5(random()::varchar);
update substit_individu set npd = md5(random()::varchar);
update substit_structure set npd = md5(random()::varchar);
update substit_unite_rech set npd = md5(random()::varchar);
