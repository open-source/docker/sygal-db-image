----------------------------------------------------------------------------------------------------------
-- Remaniement de données, et données de test.
----------------------------------------------------------------------------------------------------------

--
-- suppression de données inutiles
--
delete from substit_fk_replacement;

--
-- retraits de privilèges pour interdire de lancer import et synchro
--
delete from profil_privilege
where privilege_id = (
    select p.id from privilege p
                         join categorie_privilege cp on p.categorie_id = cp.id and cp.code = 'unicaen-db-import'
    where p.code = 'import-lancer'
);
delete from role_privilege
where privilege_id = (
    select p.id from privilege p
                         join categorie_privilege cp on p.categorie_id = cp.id and cp.code = 'unicaen-db-import'
    where p.code = 'import-lancer'
);

--
-- re-création si nécessaire de l'établissement "inconnu".
--
insert into structure(id, libelle, type_structure_id, histo_createur_id, histo_modificateur_id, source_id, source_code, code)
select nextval('structure_id_seq'),
       'Établissement inconnu',
       1, -- type etab
       1, 1,
       1, -- source sygal
       'ETAB_INCONNU', -- source code unique
       'INCONNU' -- code
on conflict do nothing;
insert into etablissement(id, structure_id, histo_createur_id, histo_modificateur_id, source_id, source_code)
select nextval('etablissement_id_seq'),
       currval('structure_id_seq'),
       1, 1,
       1, -- source sygal
       'ETAB_INCONNU' -- source code unique, idem structure
on conflict do nothing;

--
-- re-création si nécessaire de l'établissement "CED".
--
insert into structure (id, source_code, code, sigle, libelle,
                       type_structure_id, source_id, histo_createur_id)
select nextval('structure_id_seq'), 'CED', 'CED', 'CED', 'Collège des Écoles Doctorales',
       1, 1, 1
from type_structure ts
on conflict do nothing;
insert into etablissement (id, structure_id, source_id, source_code, est_ced, histo_createur_id)
select nextval('etablissement_id_seq'), s.id, 1, 'CED', true, 1
from structure s
where s.source_code = 'CED'
on conflict do nothing;

--
-- création de l'individu et du compte utilisateur LOCAL de test
--
insert into individu (id, nom_usuel, nom_patronymique, prenom1, email, source_code, supann_id, source_id, histo_createur_id)
select nextval('individu_id_seq'),
       $$Premier$$,
       $$Premier$$,
       $$François$$,
       'francois.premier@unicaen.fr',
       'UCN::00012345',
       '00012345',
       1,
       1;
insert into utilisateur (id, individu_id, username, email, display_name, password, password_reset_token, nom, prenom)
select nextval('utilisateur_id_seq'),
       i.id,
       'francois.premier@unicaen.fr', -- email, i.e. compte local
       'francois.premier@unicaen.fr',
       $$François Premier$$,
       '(Cf. ci-dessous pour choisir le mdp)', -- mdp bcrypté car compte local
       'sdfkjhWwGkhgoGHLJKgSvPBWnlCtHWIgb8GLH3iGkgKfHJmKwrKSHoGqDjHTa6T9a8',
       $$Premier$$,
       $$François$$
from individu i
where i.source_code = 'UCN::00012345';
-- NB : pour choisir le mot de passe de ce compte, aller à l'adresse
--      /utilisateur/init-compte/sdfkjhWwGkhgoGHLJKgSvPBWnlCtHWIgb8GLH3iGkgKfHJmKwrKSHoGqDjHTa6T9a8

--
-- /!\ Attribution du rôle Admin tech à l'individu/utilisateur de test !!
--
insert into individu_role(id, individu_id, role_id)
select nextval('individu_role_id_seq'), i.id, r.id
from individu i, role r
where i.source_code = 'UCN::00012345'
  and r.source_code = 'ADMIN_TECH'
;
