<?php
/**
 * Fichier de config locale, AVEC INFORMATIONS SENSIBLES.
 */

namespace Application;

use Laminas\Mail\Transport\Smtp;

return [

    'import' => [
        'connections' => [],
        'imports' => [],
        'synchros' => [],
    ],

    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'params' => [
                    'host' => '{DB_HOST}',          //
                    'port' => '{DB_PORT}',          //
                    'dbname' => '{DB_NAME}',        // remplacés par le script sh
                    'user' => '{DB_USER}',          //
                    'password' => '{DB_PASSWORD}',  //
                ],
            ],
        ],
    ],

    'cli_config' => [
        'scheme' => 'https',
        'domain' => 'localhost:8443',
    ],

    'unicaen-app' => [
        'mail' => [
            'transport' => Smtp::class,
            'transport_options' => [
                'host' => 'smtp.domain.fr',
                'port' => 25,
            ],
            'from' => 'ne_pas_repondre@etablissement-demo.fr',
            'redirect_to' => ['e.mail@etablissement-demo.fr'],
        ],
        /**
         * Paramètres de fonctionnement LDAP.
         */
        'ldap' => [
            'connection' => [
                'default' => [
                    'params' => [
                        // pas de LDAP
                    ]
                ]
            ]
        ],

        /**
         * Paramétrage pour utilisation pour autorisation ou non à la connexion à
         * une app de l'exterieur de l'établissement.
         */
        'hostlocalization' => [
            'activated' => false,
            'proxies' => [
                //xxx.xx.xx.xxx
            ],
            'reverse-proxies' => [
                //xxx.xx.xx.xxx
            ],
            'masque-ip' => '',
        ],
    ],

    'unicaen-auth' => [
        'shib' => [],
        'usurpation_allowed_usernames' => [],
    ],

    'liste-diffusion' => [
        'email_domain' => 'liste.etablissement-demo.fr',
        'sympa' => [
            'url' => 'https://liste.etablissement-demo.fr',
        ],
        'proprietaires' => [
            'e.mail@etablissement-demo.fr' => "HOCHON Paule",
        ],
    ],
];
