<?php

namespace Application;

use Admission\Entity\Db\ConventionFormationDoctorale;
use Admission\Entity\Db\Etudiant;
use Application\Entity\Db\RapportAvis;
use Application\Entity\Db\Variable;
use ComiteSuiviIndividuel\Entity\Db\Membre;
use Depot\Entity\Db\Diffusion;
use Depot\Entity\Db\MetadonneeThese;
use Depot\Entity\Db\RdvBu;
use Fichier\Entity\Db\Fichier;
use Formation\Entity\Db\EnqueteReponse;
use Formation\Entity\Db\Inscription;
use Formation\Entity\Db\Seance;
use Individu\Entity\Db\Individu;
use Application\Entity\Db\MailConfirmation;
use Individu\Entity\Db\IndividuCompl;
use RapportActivite\Entity\Db\RapportActivite;
use Soutenance\Entity\Adresse;
use Soutenance\Entity\Avis;
use Soutenance\Entity\Proposition;
use These\Entity\Db\These;
use Application\Entity\Db\Utilisateur;
use Doctorant\Entity\Db\Doctorant;
use UnicaenAvis\Entity\Db\AvisComplem;
use UnicaenDbImport\Entity\Db\ImportLog;

return [
    'unicaen-db-anonym' => [
        'output' => [
            'anonymisation' => '/tmp/demo.unicaen_db_anonym_anonymisation.sql',
            'restauration' => '/tmp/demo.unicaen_db_anonym_restauration.sql', // pas utilisé pour générer la bddd de démo
        ],
        'entities' => [
            ConventionFormationDoctorale::class => [
                'mapping' => [
                    'calendrier_projet_recherche' => 'paragraph',
                    'modalites_encadr_suivi_avancmt_rech' => 'paragraph',
                    'conditions_realisation_proj_rech' => 'paragraph',
                    'modalites_integration_ur' => 'paragraph',
                    'partenariats_proj_these' => 'paragraph',
                    'motivation_demande_confidentialite' => 'paragraph',
                    'projet_pro_doctorant' => 'paragraph',
                ],
            ],
            Etudiant::class => [
                'mapping' => [
                    'sexe' => 'title',
                    'nom_usuel' => 'lastName',
                    'nom_famille' => 'lastName',
                    'prenom' => 'firstName',
                    'prenom2' => 'null',
                    'prenom3' => 'null',
                    'ine' => ['randomNumber', 6, true],
                    'courriel' => 'safeEmail',
                    'adresse_ligne1_etage' => 'streetAddress',
                    'adresse_ligne2_batiment' => 'secondaryAddress',
                    'adresse_ligne3_voie' => 'null',
                    'adresse_ligne4_complement' => 'null',
                    'adresse_code_postal' => ['randomNumber', 5, true],
                    'adresse_nom_commune' => 'null',
                    'adresse_code_commune' => 'null',
                    'code_commune_naissance' => 'null',
                    'libelle_commune_naissance' => 'null',
                    'adresse_cp_ville_etrangere' => 'null',
                    'numero_telephone1' => 'null',
                    'numero_telephone2' => 'null',
                ],
            ],
            \Admission\Entity\Db\Inscription::class => [
                'mapping' => [
                    'prenom_directeur_these' => 'firstName',
                    'nom_directeur_these' => 'lastName',
                    'mail_directeur_these' => 'safeEmail',
                    'prenom_codirecteur_these' => 'firstName',
                    'nom_codirecteur_these' => 'lastName',
                    'mail_codirecteur_these' => 'safeEmail',
                    'titre_these' => 'sentence',
                ],
            ],

            Membre::class => [
                'mapping' => [
                    'email' => 'safeEmail',
                    'nom' => 'lastName',
                    'prenom' => 'firstName',
                ]
            ],
            Diffusion::class => [
                'mapping' => [
                    'autoris_motif' => 'sentence',
                    'orcid' => 'md5',
                    'nnt' => ['name' => 'isbn13'], // varchar(30) !
                    'hal_id' => 'md5',
                ]
            ],
            Doctorant::class => [
                'mapping' => [
                    'source_code' => ['name' => 'md5', 'unique' => true], // todo : faut-il préserver le prefixe étab ?
                    'ine' => 'md5',
                    'npd_force' => 'md5',
                ],
                'except' => [
                    //'source' => [2, 4, 5],
                ],
            ],
            Fichier::class => [
                'mapping' => [
                    'nom' => 'word',
                    'nomOriginal' => 'word',
                ],
            ],
            EnqueteReponse::class => [
                'mapping' => [
                    'description' => 'sentence',
                ],
            ],
            Inscription::class => [
                'mapping' => [
                    'description' => 'sentence',
                ],
            ],
            Seance::class => [
                'mapping' => [
                    'lieu' => 'address',
                ],
            ],
            Individu::class => [
                'mapping' => [
                    'source_code' => ['name' => 'md5', 'unique' => true], // todo : faut-il préserver le prefixe étab ?
                    'civilite' => ['name' => 'randomElement', 'params' => [[Individu::CIVILITE_MME, Individu::CIVILITE_M]]],
                    'nomUsuel' => 'lastName',
                    'nomPatronymique' => 'lastName',
                    'prenom1' => ['name' => 'firstName', 'params' => [null], 'context' => true],
                    'prenom2' => 'null',
                    'prenom3' => 'null',
                    'email' => ['name' => 'numerify', 'unique' => true, 'params' => ['mail######@etab.fr']],
                    'supann_id' => 'isbn13', // varchar(30) !
                    'id_ref' => 'md5',
                    'npd_force' => 'md5',
                ],
                'except' => [
                    'id' => [
                        4974, // Bertrand
                        97521, // Nicolas
                        1183582, // Thomas
                    ],
                ],
            ],
            IndividuCompl::class => [
                'mapping' => [
                    'email' => ['name' => 'numerify', 'unique' => true, 'params' => ['mail######@perso.fr']],
                ],
            ],
            MailConfirmation::class => [
                'mapping' => [
                    'email' => 'safeEmail',
                ],
            ],
            MetadonneeThese::class => [
                'mapping' => [
                    'titre' => 'sentence',
                    'titre_autre_langue' => 'sentence',
                    'resume' => 'paragraph',
                    'resume_anglais' => 'paragraph',
                    'mots_cles_libres_fr' => ['words', 4, true],
                    'mots_cles_libres_ang' => ['words', 4, true],
                ],
            ],
            RapportActivite::class => [
                'mapping' => [
                    'productions_scientifiques' => 'paragraph',
                    'par_directeur_these_motif' => 'sentence',
                    'principaux_resultats_obtenus' => 'paragraph',
                    'description_projet_recherche' => 'paragraph',
                    'actions_diffusion_culture_scientifique' => 'paragraph',
                    'autres_activites' => 'paragraph',
                    'calendrier_previonnel_finalisation' => 'paragraph',
                    'preparation_apres_these' => 'paragraph',
                    'perspectives_apres_these' => 'paragraph',
                    'commentaires' => 'sentence',
                ],
            ],
            RdvBu::class => [
                'mapping' => [
                    'coord_doctorant' => 'sentence',
                    'dispo_doctorant' => ['randomElement', ["Le soir", "Le matin"]],
                    'mots_cles_rameau' => ['words', 4, true],
                    'divers' => 'sentence',
                ],
            ],
            Adresse::class => [
                'mapping' => [
                    'ligne1' => 'secondaryAddress',
                    'ligne2' => 'streetAddress',
                    'ligne3' => 'null',
                ],
            ],
            Avis::class => [
                'mapping' => [
                    'motif' => 'sentence',
                ],
            ],
            \Soutenance\Entity\Membre::class => [
                'mapping' => [
                    'email' => 'safeEmail',
                    'nom' => 'lastName',
                    'prenom' => 'firstName',
                    'adresse' => 'address',
                ],
            ],
            Proposition::class => [
                'mapping' => [
                    'lieu' => 'address',
                    'nouveau_titre' => 'sentence',
                    'adresse_exacte' => 'address',
                ],
            ],
            These::class => [
                'mapping' => [
                    'titre' => 'sentence',
                ],
            ],
            AvisComplem::class => [
                'mapping' => [
                    'valeur' => 'sentence',
                ],
            ],
            Utilisateur::class => [
                'mapping' => [
                    'username' => ['name' => 'numerify', 'unique' => true, 'params' => ['user########']],
                    'email' => 'safeEmail',
                    'displayName' => 'name',
                    'nom' => 'lastName',
                    'prenom' => 'firstName',
                ],
                'except' => [
                    'username' => [
                        'sygal-app',
                    ],
                    'id' => [
                        1442, // Bertrand
                        7608, // Nicolas
                        143423, // Thomas
                        3507, // Véro
                    ],
                ],
            ],
            \Application\Entity\Db\ValiditeFichier::class => [
                'mapping' => [
                    'log' => 'sentence',
                ],
            ],
            Variable::class => [
                'mapping' => [
                    'valeur' => 'safeEmail',
                ],
                'except' => [
                    'code' => [
                        'ETB_ART_ETB_LIB',
                        'ETB_LIB',
                        'ETB_LIB_NOM_RESP',
                        'ETB_LIB_TIT_RESP',
                        'TRIBUNAL_COMPETENT',
                    ],
                ],
            ],
        ],
    ],
];
