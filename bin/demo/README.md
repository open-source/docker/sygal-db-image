Mise à jour du dump SQL de création d'une bdd de **démo**
=========================================================

Idée
----

L'idée est de lancer le script de génération puis de mise à jour du dump SQL permettant de créer une bdd de 
démo à partir d'une bdd modèle à laquelle on doit pouvoir se connecter.

Les étapes de ce script sont les suivantes :
  - Dump SQL de la bdd modèle (pg_dump).
  - Lancement d'une bdd temporaire (à partir de l'image Docker 'sygal-db-image').
  - Exécution du script SQL de peuplement de la bdd temporaire (import du dump SQL)
  - Lancement de la procédure d'élagage des données en bdd temporaire.
  - Génération du script SQL d'anonymisation des données de la bdd temporaire (ligne de commande de SyGAL).
  - Lancement du script SQL d'anonymisations complémentaires de la bdd temporaire.
  - Lancement du script SQL de préparations diverses de la bdd temporaire.
  - Dump SQL de la bdd temporaire (pg_dump).
  - Copie du dump SQL dans le répertoire cible.

**NB : l'exécution de ce script peut durer jusqu'à 20 minutes.**


Utilisation
-----------

Le script peut être lancer de n'importe quel emplacement.

Il s'attend à trouver les variables d'environnement permettant de se connecter à la bdd modèle.

Exemple :

```bash
PGHOST=localhost \
PGDATABASE=sygal \
PGPORT=5401 \
PGUSER=ad_sygal \
PGPASSWORD=azerty \
bin/demo/update_demo.sh 
```

Un répertoire temporaire de travail sera créé par le script pour y mettre les différents artefacts générés
mais il est possible de spécifier un répertoire existant à utiliser.

Exemple :

```bash
PGHOST=localhost \
PGDATABASE=sygal \
PGPORT=5401 \
PGUSER=ad_sygal \
PGPASSWORD=azerty \
bin/demo/update_demo.sh --tmp /tmp/tmp.iiNxgqBUyK
```

Si le répertoire temporaire est spécifié et que certains artefacts résultant d'un précédent lancement y sont trouvés, 
certaines étapes seront sautées :
  - Dump SQL de la bdd modèle (pg_dump).
  - Exécution des scripts SQL de peuplement de la bdd SyGAL temporaire : import du dump puis élagage des données.
  - Génération du script SQL d'anonymisation des données de la bdd SyGAL temporaire (ligne de commande de SyGAL).
