#!/bin/bash
set -o nounset
#set -o xtrace

THIS_DIR=$(cd $(dirname $0) && pwd)
ROOT_DIR=${THIS_DIR}

###############################################################################################################
#
# Le but de ce script est de mettre à jour les scripts SQL présents dans cette image ; scripts permettant
# de créer/initialiser une base de données vierge pour l'application SyGAL.
#
# Il récupère dans les sources de l'application SyGAL (à partir d'un dépôt git ou d'un répertoire local)
# les scripts et ressources permettant de générer les scripts SQL en question.
#
###############################################################################################################

TARGET_DIR=$(realpath "${ROOT_DIR}/../../docker/demo")

SYGAL_BRANCH="master"

SYGAL_DB="sygal"
SYGAL_USER="ad_sygal"
SYGAL_PASSWORD="azerty"


usage() {
  echo "Usage :"
  echo "    $(basename $0) ARGUMENTS"
  echo "Arguments :"
  echo "-t|--tmp <DIR> Répertoire temporaire à (ré)utiliser. Facultatif."
  exit 0
}

#if [ ${#} -eq 0]]; then
#  usage
#fi

TMP_DIR=$(mktemp -d)

DOCKER_CLI_HINTS=false
DB_CONTAINER_NAME="sygal_db_$RANDOM"
NETWORK_NAME="sygal_network_$RANDOM"

ARGS=`getopt -o "g:l:" -l "t:,x:,tmp:,xxx:" -n "getopt.sh" -- "$@"`
[[ $? -ne 0 ]] && exit 1
eval set -- "$ARGS"
while true;
do
  case "$1" in
    -t|--tmp)
      [[ -n "$2" ]] && TMP_DIR="$2"
      shift 2;;
#    -x|--xxx)
#      [[ -n "$2" ]] && DUMP_DIR="$2"
#      shift 2;;
    --)
      shift
      break;;
    ?)
      echo "Option invalide : -${OPTARG}."
      usage;;
  esac
done

teardown() {
  docker stop $DB_CONTAINER_NAME >/dev/null 2>&1
  docker rm $DB_CONTAINER_NAME >/dev/null 2>&1
  docker network rm $NETWORK_NAME >/dev/null 2>&1
  return 0
}

trap 'teardown; exit 1' EXIT HUP INT TERM
trap 'teardown' EXIT


echo "=================================================================================="
echo "             Génération des scripts SQL de création d'une bdd de démo             "
echo "=================================================================================="

[[ ! -d $TMP_DIR ]] && echo "Le répertoire $TMP_DIR doit exister au préalable" && exit 1

[[ -z $PGHOST ]] && echo "Vous devez fournir la variable PGHOST." && exit 1
[[ -z $PGPORT ]] && echo "Vous devez fournir la variable PGPORT." && exit 1
[[ -z $PGDATABASE ]] && echo "Vous devez fournir la variable PGDATABASE." && exit 1
[[ -z $PGUSER ]] && echo "Vous devez fournir la variable PGUSER." && exit 1
[[ -z $PGPASSWORD ]] && echo "Entrez la valeur de PGPASSWORD, svp. " && read -s PGPASSWORD && export PGPASSWORD

echo "# Répertoire temporaire : $TMP_DIR"

echo "--------------------------------------------------------------------------------------------------------"

SQL_OUTPUT_DIR="$TMP_DIR/sql"
mkdir -p $SQL_OUTPUT_DIR

#
# Dump de la bdd modèle.
#
SQL_DUMP_FILE_NAME="02_dump.sql"
SQL_DUMP_FILE_PATH="$SQL_OUTPUT_DIR/$SQL_DUMP_FILE_NAME"
echo "# Dump SQL de la bdd modèle $PGUSER/$PGDATABASE@$PGHOST:$PGPORT... "
if [ ! -f "$SQL_DUMP_FILE_PATH" ]; then
  docker run \
    --env PGHOST \
    --env PGPORT \
    --env PGDATABASE \
    --env PGUSER \
    --env PGPASSWORD \
    --net host \
    postgres:15 \
      pg_dump \
        --exclude-table z_* \
        --exclude-table sav_* \
        --exclude-table *_sav \
        --exclude-table-data notif_mail \
        --exclude-table-data liste_diff \
        --exclude-table-data unicaen_renderer_rendu \
        --exclude-table-data user_token \
        --exclude-table-data tmp_* \
        --exclude-table-data *_log \
        --no-owner \
      > "$SQL_DUMP_FILE_PATH"
  [[ $? -ne 0 ]] && exit 1
  echo "Fait : $SQL_DUMP_FILE_PATH"
else
  echo "Inutile, un fichier $SQL_DUMP_FILE_PATH existe déjà."
fi
unset POSTGRES_PASSWORD PGHOST PGPORT PGDATABASE PGUSER PGPASSWORD

echo "--------------------------------------------------------------------------------------------------------"

#
# Lancement d'une bdd temporaire minimale.
#
echo "# Lancement en arrière-plan d'une bdd temporaire vide $SYGAL_USER/$SYGAL_DB... "
GIT_DIR="$TMP_DIR/docker/sygal-db-image"
if [ ! -d "$GIT_DIR" ]; then
  mkdir -p "$TMP_DIR/docker" && cd "$TMP_DIR/docker" && \
  git clone git@git.unicaen.fr:open-source/docker/sygal-db-image && \
  cd $GIT_DIR && \
  git fetch && git checkout master && git pull
  [[ $? -ne 0 ]] && exit 1
fi
cd $GIT_DIR && \
rm -f ./docker/sql/*.sql && \
docker network create -d bridge $NETWORK_NAME && \
docker build --build-arg RESOURCE_DIR="docker/empty" -t sygal-db-image . && \
docker run \
  -d \
  --env POSTGRES_PASSWORD=admin \
  --env SYGAL_DB=$SYGAL_DB \
  --env SYGAL_USER=$SYGAL_USER \
  --env SYGAL_PASSWORD=$SYGAL_PASSWORD \
  --publish "55432:5432" \
  --volume $SQL_OUTPUT_DIR:/tmp/sql \
  --volume $TMP_DIR/docker/db:/var/lib/postgresql/data \
  --network $NETWORK_NAME \
  --name $DB_CONTAINER_NAME \
  sygal-db-image
[[ $? -ne 0 ]] && exit 1
echo "En cours dans le container '$DB_CONTAINER_NAME'"
echo "Attente que la bdd temporaire soit créée et prête..."
timeout 60s bash -c "until docker exec $DB_CONTAINER_NAME pg_isready --host=localhost ; do sleep 5 ; done"
[[ $? -ne 0 ]] && docker container logs $DB_CONTAINER_NAME && echo 'Trop long, abandon !' && exit 1

echo "--------------------------------------------------------------------------------------------------------"

cp -f $ROOT_DIR/src/db/*.sql "$SQL_OUTPUT_DIR/"

SQL_SETUP_FILE_NAME="03_setup.sql"
SQL_PRUNE_FILE_NAME="04_prune.sql"

#
# Exécution des scripts de peuplement de la bdd temporaire, si aucune donnée déjà persistée.
#
echo "# Exécution des scripts de peuplement de la bdd temporaire $SYGAL_USER/$SYGAL_DB..."
echo "  - Lancement de $SQL_DUMP_FILE_NAME..." && \
docker exec \
  --env PGDATABASE=$SYGAL_DB \
  --env PGUSER=$SYGAL_USER \
  $DB_CONTAINER_NAME \
    psql -o /dev/null --quiet -v ON_ERROR_STOP=1 -f /tmp/sql/$SQL_DUMP_FILE_NAME && \
\
echo "  - Lancement de $SQL_SETUP_FILE_NAME..." && \
docker exec \
  --env PGDATABASE=$SYGAL_DB \
  --env PGUSER=$SYGAL_USER \
  $DB_CONTAINER_NAME \
    psql -o /dev/null --quiet -v ON_ERROR_STOP=1 -f /tmp/sql/$SQL_SETUP_FILE_NAME && \
\
echo "  - Lancement de $SQL_PRUNE_FILE_NAME..." && \
docker exec \
  --env PGDATABASE=$SYGAL_DB \
  --env PGUSER=$SYGAL_USER \
  $DB_CONTAINER_NAME \
    psql -o /dev/null --quiet -v ON_ERROR_STOP=1 -f /tmp/sql/$SQL_PRUNE_FILE_NAME && \
echo "Fait."
[[ $? -ne 0 ]] && exit 1

echo "--------------------------------------------------------------------------------------------------------"

#
# Build et run SyGAL pour générer le script d'anonymisation sur la base temporaire élaguée.
#
SQL_OUTPUT_FILE_NAME="09_anonymisation.sql"
SQL_OUTPUT_FILE_PATH="$SQL_OUTPUT_DIR/$SQL_OUTPUT_FILE_NAME"
echo "# Génération du script d'anonymisation des données de la bdd temporaire $SYGAL_USER/$SYGAL_DB... "
if [ ! -f "$SQL_OUTPUT_FILE_PATH" ]; then
  GIT_DIR="$TMP_DIR/docker/sygal"
  if [ ! -d "$GIT_DIR" ]; then
    mkdir -p "$TMP_DIR/docker" && cd "$TMP_DIR/docker" && \
    git clone git@git.unicaen.fr:open-source/sygal
  fi
  [[ $? -ne 0 ]] && exit 1
  cd $GIT_DIR && git checkout $SYGAL_BRANCH && git pull
  [[ $? -ne 0 ]] && exit 1
  docker build --build-arg PHP_VERSION=8.0 -t sygal-image-tmp . && \
  echo "Attente que la bdd temporaire soit prête..." && \
  timeout 120s bash -c "until docker exec $DB_CONTAINER_NAME pg_isready --host=localhost ; do sleep 5 ; done"
  [[ $? -ne 0 ]] && docker container logs $DB_CONTAINER_NAME && exit 1
  cp -r $ROOT_DIR/src/app/zzz.local.php $TMP_DIR/ && \
  cp -r $ROOT_DIR/src/app/zzz.secret.local.php $TMP_DIR/ && \
  cp -r $ROOT_DIR/src/app/zzz.unicaen-db-anonym.local.php $TMP_DIR/ && \
  sed -i "s|{DB_HOST}|$DB_CONTAINER_NAME|g
          s|{DB_PORT}|5432|g
          s|{DB_NAME}|$SYGAL_DB|g
          s|{DB_USER}|$SYGAL_USER|g
          s|{DB_PASSWORD}|$SYGAL_PASSWORD|g" $TMP_DIR/zzz.secret.local.php && \
  docker run \
    --rm \
    -v $TMP_DIR/zzz.local.php:/app/config/autoload/zzz.local.php:ro \
    -v $TMP_DIR/zzz.secret.local.php:/app/config/autoload/zzz.secret.local.php:ro \
    -v $TMP_DIR/zzz.unicaen-db-anonym.local.php:/app/config/autoload/zzz.unicaen-db-anonym.local.php:ro \
    -v $HOME/.cache/composer:/root/.cache/composer \
    -v $HOME/.config/composer:/root/.config/composer \
    -v /tmp:/tmp \
    -w /app \
    --network $NETWORK_NAME \
    sygal-image-tmp \
      bash -c "rm -f /tmp/demo.unicaen_db_anonym_*.sql && php public/index.php unicaen-db-anonym generer" && \
  cp -f /tmp/demo.unicaen_db_anonym_anonymisation.sql $SQL_OUTPUT_FILE_PATH && \
  echo "Fait." &&
  echo "> $SQL_OUTPUT_FILE_PATH"
  [[ $? -ne 0 ]] && exit 1
else
  echo "Inutile, un fichier $SQL_OUTPUT_FILE_PATH existe déjà."
fi

echo "--------------------------------------------------------------------------------------------------------"

#
# Exécution du script d'anonymisation généré sur la bdd temporaire.
#
echo "# Exécution du script d'anonymisation sur la bdd temporaire $SYGAL_USER/$SYGAL_DB... "
docker exec \
  --env PGDATABASE=$SYGAL_DB \
  --env PGUSER=$SYGAL_USER \
  $DB_CONTAINER_NAME \
    psql --quiet -v ON_ERROR_STOP=1 -f "/tmp/sql/$SQL_OUTPUT_FILE_NAME" && \
echo "Fait."
[[ $? -ne 0 ]] && exit 1

echo "--------------------------------------------------------------------------------------------------------"

SQL_POST_ANONYM_FILE_NAME="05_post_anonym.sql"

#
# Exécution du script post-anonymisation de la bdd temporaire.
#
echo "# Exécution du script post-anonymisation de la bdd temporaire $SYGAL_USER/$SYGAL_DB..."
#if [ ! -d "$TMP_DIR/docker/db" ]; then
  echo "    - Lancement de $SQL_POST_ANONYM_FILE_NAME..." && \
  docker exec \
    --env PGDATABASE=$SYGAL_DB \
    --env PGUSER=$SYGAL_USER \
    $DB_CONTAINER_NAME \
      psql -o /dev/null --quiet -v ON_ERROR_STOP=1 -f /tmp/sql/$SQL_POST_ANONYM_FILE_NAME && \
  echo "Fait."
  [[ $? -ne 0 ]] && exit 1
#else
#  echo "Inutile, des données persistées existent déjà dans $TMP_DIR/docker/db."
#fi

echo "--------------------------------------------------------------------------------------------------------"

SQL_PREPARE_FILE_NAME="06_prepare.sql"

#
# Exécution du script de préparation de la bdd temporaire.
#
echo "# Exécution du script de preparation de la bdd temporaire $SYGAL_USER/$SYGAL_DB..."
#if [ ! -d "$TMP_DIR/docker/db" ]; then
  echo "    - Lancement de $SQL_PREPARE_FILE_NAME..." && \
  docker exec \
    --env PGDATABASE=$SYGAL_DB \
    --env PGUSER=$SYGAL_USER \
    $DB_CONTAINER_NAME \
      psql -o /dev/null --quiet -v ON_ERROR_STOP=1 -f /tmp/sql/$SQL_PREPARE_FILE_NAME && \
  echo "Fait."
  [[ $? -ne 0 ]] && exit 1
#else
#  echo "Inutile, des données persistées existent déjà dans $TMP_DIR/docker/db."
#fi

echo "--------------------------------------------------------------------------------------------------------"

#
# Dump SQL de la bdd temporaire.
#
SQL_OUTPUT_DIR="$TMP_DIR/sql"
SQL_DUMP_FILE_PATH="$SQL_OUTPUT_DIR/02_dump.sql"
mkdir -p $SQL_OUTPUT_DIR
echo "# Dump SQL de la bdd anonymisée... "
docker exec \
  --env PGDATABASE=$SYGAL_DB \
  --env PGUSER=$SYGAL_USER \
  $DB_CONTAINER_NAME \
    pg_dump --no-owner > $SQL_DUMP_FILE_PATH && \
echo "Fait." && \
ls -lh $SQL_DUMP_FILE_PATH
[[ $? -ne 0 ]] && exit

echo "--------------------------------------------------------------------------------------------------------"

echo "# Copie du dump SQL dans le répertoire $TARGET_DIR/sql/02_other/ :"
cp -v --backup --suffix="~" $SQL_DUMP_FILE_PATH "$TARGET_DIR/sql/02_other/"

echo "--------------------------------------------------------------------------------------------------------"

echo "TERMINÉ"
