#!/bin/bash
set -o nounset
set -e

THIS_DIR=$(cd $(dirname $0) && pwd)
ROOT_DIR=${THIS_DIR}

###############################################################################################################
#
# Le but de ce script est de mettre à jour les scripts SQL présents dans cette image ; scripts permettant
# de créer/initialiser une base de données vierge pour l'application SyGAL.
#
# Il récupère dans les sources de l'application SyGAL (à partir d'un dépôt git ou d'un répertoire local)
# les scripts et ressources permettant de générer les scripts SQL en question.
#
###############################################################################################################

TARGET_DIR=$(realpath "${ROOT_DIR}/../../docker/dist")

usage() {
  echo "Usage :"
  echo "    $(basename $0) ARGUMENTS"
  echo "Arguments :"
  echo "-g|--git GIT_REPO
    Adresse du dépôt git de SyGAL.
    Facultatif si -l est utilisé.
    Ex : git@git.unicaen.fr:/open-source/sygal"
  echo "-l|--local LOCAL_DIR
    Chemin local des sources de SyGAL, en remplacement du dépôt git.
    Facultatif si -g est utilisé."
  exit 0
}

if [[ ${#} -eq 0 ]]; then
  usage
fi

ARGS=`getopt -o "g:l:" -l "git:,local:" -n "getopt.sh" -- "$@"`
[[ $? -ne 0 ]] && exit 1
eval set -- "$ARGS"
while true;
do
  case "$1" in
    -g|--git)
      [[ -n "$2" ]] && GIT_REPO="$2"
      shift 2;;
    -l|--local)
      [[ -n "$2" ]] && LOCAL_DIR="$2"
      shift 2;;
    --)
      shift
      break;;
    ?)
      echo "Option invalide : -${OPTARG}."
      usage;;
  esac
done


TMP_DIR=$(mktemp -d)
BRANCH=master

[[ ! -d $TMP_DIR ]] && echo "Le répertoire $TMP_DIR doit être créé au préalable" && exit 1
[[ ! -z "$(ls -A $TMP_DIR)" ]] && echo "Le répertoire $TMP_DIR doit être vide" && exit 1
[[ -n ${LOCAL_DIR} ]] && [[ ! -d $LOCAL_DIR ]] && echo "Le répertoire $LOCAL_DIR n'existe pas" && exit 1

if [[ -n ${GIT_REPO} ]]; then
  echo
  echo "----- Obtention des dernières versions des scripts de création de bdd via ${GIT_REPO} ----"
  git clone ${GIT_REPO} ${TMP_DIR}
  cd ${TMP_DIR}
  git checkout ${BRANCH}
  git pull
elif [[ -n ${LOCAL_DIR} ]]; then
  echo
  echo "----- Copie des scripts de création de bdd à partir du répertoire local ${LOCAL_DIR} ----"
  mkdir -p ${TMP_DIR}/doc/database
  cp -rv ${LOCAL_DIR}/doc/database/* ${TMP_DIR}/doc/database/
fi

echo
echo "----- Préparation des scripts SQL ---------------------------------------------------------------"
cd ${TMP_DIR}/doc/database
mv build_db_files.conf.dist build_db_files.conf
bash build_db_files.sh -c ./build_db_files.conf -i ./sql/
rm ./sql/*.dist

echo
echo "----- Mise à jour des scripts SQL dans l'image-------------------------------------------------"
rm -r ${TARGET_DIR}/sql/*
cp -rv ./sql/* ${TARGET_DIR}/sql/

echo
echo "Terminé."
echo "Faites un git status pour voir les changements !"

cd ${ROOT_DIR}
