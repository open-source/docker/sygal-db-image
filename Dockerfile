###########################################################################################
#
#                           Base de données PostgreSQL de SyGAL.
#
###########################################################################################

FROM postgres:15

LABEL maintainer="Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>"

#
# Argument spécifiant l'emplacement des ressources Docker à utiliser (valeur par défaut : ressources pour une
# base de données **neuve**).
#
ARG RESOURCE_DIR="docker/dist"

RUN localedef -i fr_FR -c -f UTF-8 -A /usr/share/locale/locale.alias fr_FR.UTF-8

ENV LANG="fr_FR.utf8"
ENV TZ="Europe/Paris"
ENV PGTZ="Europe/Paris"

#
# Copie des ressources à utiliser pour créer la base de données :
#   - Scripts SQL qui seront lancés par le script `initdb.sh` (ci-après).
#   - Script `initdb.sh` lancé automatiquement ssi le répertoire de persistance
#     de la base de données est vide.
#
# NB : il est aussi possible de substituer ces ressources par les vôtres grâce à des montages de volumes.
#
COPY ${RESOURCE_DIR}/sql/ /sql/
COPY ${RESOURCE_DIR}/initdb.sh /docker-entrypoint-initdb.d/
